import javax.imageio.ImageIO;
import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import java.io.File;

public class NewAccountApplet extends JApplet implements ActionListener{
   /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JLabel titlePage; 
	JLabel[] txt; 
	JTextField[] jtf; 
	JTextField[] quantity;
	JButton accept, decline, purchase, cancel;
	JPanel jp1, jp2, jp3, jp4, jp5, jp6, jp7, jp8, jp9;
	String[] accountlist = {"Select Account Type.", "Customer", "Admin"};
	JComboBox<Object> textAlignment = new JComboBox<Object>(accountlist);
	GridLayout productLO = new GridLayout(10,10,10,10);
	GridLayout productMO = new GridLayout(10,4,10,10);
	String[] qtyArray = {"1", "2", "3", "4", "5", "6"};
	int qty = 9;
    JComboBox<Object>[] selectQty, selectQty2;
    String[] itemText = {"White Snapback        $35", "Silver Necklace        $125", "Black T Shirt             $45", "Blue Jean                 $105", "Black Jean                $105", "Blue Sneakers             $85", "Black Sneakers            $85", "Blousson                   $280"};
	JLabel[] items = new JLabel[itemText.length];
	JLabel purchasePage = new JLabel("Items for Purchase");
	String[] kind = {"White Snapback", "Silver Necklace", "Black T Shirt", "Blue Jean", "Black Jean", "Blue Sneakers", "Black Sneakers", "Blousson"};
	JLabel[] kinds = new JLabel[kind.length];
	
   public void init(){
	   	setSize(400,400);
	   	JPanel content = (JPanel)getContentPane(); 
		GridBagConstraints firstCol = new GridBagConstraints(); 
		firstCol.weightx = 1.0; 
		firstCol.anchor = GridBagConstraints.WEST; 
		firstCol.insets = new Insets(5, 20, 5, 5); 
		GridBagConstraints lastCol = new GridBagConstraints(); 
		lastCol.gridwidth = GridBagConstraints.REMAINDER; 
		lastCol.weightx = 1.0; 
		lastCol.fill = GridBagConstraints.HORIZONTAL; 
		lastCol.insets = new Insets(5, 5, 5, 20); 
				
		String[] labeltxt = {"Name","Account ID","Password","E-Mail","Phone","Address","","","Account Type"}; 
		titlePage = new JLabel("Create New Account"); 
		txt = new JLabel[9]; 
		jtf = new JTextField[9]; 
		accept = new JButton("Create"); 
		decline = new JButton("Decline"); 
		
		jp1 = new JPanel(); 
		jp2 = new JPanel(new GridBagLayout()); 
		jp3 = new JPanel(); 
		jp4 = new JPanel();
		jp5 = new JPanel(new GridBagLayout()); 
		jp6 = new JPanel();
		jp7 = new JPanel();
		jp8 = new JPanel(new GridBagLayout()); 
		jp9 = new JPanel();
		
		
		for(int i=0; (i<9); i++) { 
			txt[i] = new JLabel(); 
			txt[i].setText(labeltxt[i]); 
			jp2.add(txt[i], firstCol); 
			jtf[i] = new JTextField(); 
			jtf[i].setPreferredSize(new Dimension(300, 20)); 
			jp2.add(jtf[i], lastCol); 
			
			}

		jp1.add(titlePage); 
		jp3.add(accept); 
		jp3.add(decline); 
		content.setLayout(new BoxLayout(content, BoxLayout.Y_AXIS)); 
		content.add(jp1); 
		content.add(jp2); 
		content.add(jp3); 
		String id = this.jtf[1].getText();
		String pw = this.jtf[2].getText();
		jtf[6].setText(id);
		jtf[7].setText(pw);
		
		jtf[6].setVisible(false);
		jtf[7].setVisible(false);
		jtf[8].setVisible(false);
		
		jp2.add(textAlignment, lastCol);
				
		decline.addActionListener(this);
		accept.addActionListener(this);
		
   }
   public void actionPerformed(ActionEvent e) {
		String id = jtf[1].getText();
	   	String pw = jtf[2].getText();
	   	String checkID = jtf[6].getText();
	   	String checkPW = jtf[7].getText();
		String chosenType = (String) textAlignment.getSelectedItem();
		
		JPasswordField pField = new JPasswordField(10);
	    JPanel pPanel = new JPanel();
	    pPanel.add(new JLabel("Please Enter Password: "));
	    pPanel.add(pField);
	   	
	   	if (e.getActionCommand().equals("Create") && (chosenType.equals("Customer"))){
	   		JOptionPane.showMessageDialog(null, "Thank you for Joining!");
	   		id = jtf[1].getText();
			pw = jtf[2].getText();
			titlePage.setText("Welcome to Final Sales!");
			accept.setText("Login");
			decline.setText("Cancel");
			txt[6].setText("UserName");
			txt[7].setText("Password");
			jtf[0].setText("");
			jtf[3].setText("");
			jtf[4].setText("");
			jtf[5].setText("");
			
			txt[0].setVisible(false);
			txt[1].setVisible(false);
			txt[2].setVisible(false);
			txt[3].setVisible(false);
			txt[4].setVisible(false);
			txt[5].setVisible(false);
			
			textAlignment.setVisible(false);
			txt[8].setVisible(false);
			
			jtf[0].setVisible(false);
			jtf[1].setVisible(false);
			jtf[2].setVisible(false);
			jtf[3].setVisible(false);
			jtf[4].setVisible(false);
			jtf[5].setVisible(false);
			jtf[6].setVisible(true);
			jtf[7].setVisible(true);
			
	   		}
	   	
	   	if (e.getActionCommand().equals("Create") && (chosenType.equals("Admin"))) {
	   		JOptionPane.showMessageDialog(null, pPanel);
	   		JOptionPane.showMessageDialog(null, "Wrong Admin Password"); 
	   	}
	   		
	   	if (e.getActionCommand().equals("Create") && (chosenType.equals("Select Account Type."))) {
	   		JOptionPane.showMessageDialog(null, "You have selected wrong account type.");
	   	}
	   	
		if (e.getActionCommand().equals("Decline"))
			System.exit(0);
		
		if (e.getActionCommand().equals("Login")) {	
				if (id.equals(checkID) && pw.equals(checkPW)) {
				JOptionPane.showMessageDialog(null, "Authenticated");
							
				JPanel content = (JPanel)getContentPane(); 
				GridBagConstraints firstCol = new GridBagConstraints(); 
				firstCol.weightx = 1.0; 
				firstCol.anchor = GridBagConstraints.WEST; 
				firstCol.insets = new Insets(5, 20, 5, 5);
				GridBagConstraints lastCol = new GridBagConstraints(); 
				lastCol.gridwidth = GridBagConstraints.REMAINDER; 
				lastCol.weightx = 1.0; 
				lastCol.fill = GridBagConstraints.HORIZONTAL; 
				lastCol.insets = new Insets(5, 5, 5, 10); 
				
				jp1.setVisible(false);
				jp2.setVisible(false);
				jp3.setVisible(false);
				jp4.setVisible(true);
				jp5.setVisible(true);
				jp6.setVisible(true);
				
				
				jp4.add(purchasePage);
				quantity = new JTextField[9];
				selectQty = new JComboBox[itemText.length];
				jp4 = new JPanel();
				jp5.setLayout(new GridBagLayout());
				jp6 = new JPanel();
				
				for(int i=0; (i<items.length); i++) { 
	                items[i] = new JLabel(); 
	                items[i].setText(itemText[i]); 
	                jp5.add(items[i], firstCol); 
	                selectQty[i] = new JComboBox<Object>();
	                selectQty[i].addItem("0");
	                selectQty[i].addItem("1");
	                selectQty[i].addItem("2");
	                selectQty[i].addItem("3");
	                selectQty[i].addItem("4");
	                selectQty[i].addItem("5");
	                selectQty[i].addItem("6");
	                selectQty[i].addItem("7");
	                selectQty[i].addItem("8");
	                selectQty[i].addItem("9");
	                selectQty[i].setPreferredSize(new Dimension(20, 20)); 
	                jp5.add(selectQty[i], lastCol); 
	            }

				content.setLayout(new BoxLayout(content, BoxLayout.Y_AXIS)); 
				content.add(jp4); 
				content.add(jp5); 
				content.add(jp6); 
				jp4.add(purchasePage);
				accept.setText("Purchase");
				decline.setText("Cancel");
				
				jp6.add(accept);
				jp6.add(decline);
				
				}
				
				else JOptionPane.showMessageDialog(null, "Wrong account information");
				}
			if (e.getActionCommand().equals("Purchase")) {
				JOptionPane.showMessageDialog(null, "Transaction Complete");
				JPanel content = (JPanel)getContentPane();
				GridBagConstraints firstCol = new GridBagConstraints(); 
				firstCol.weightx = 1.0; 
				firstCol.anchor = GridBagConstraints.WEST; 
				firstCol.insets = new Insets(5, 20, 5, 5);
				GridBagConstraints lastCol = new GridBagConstraints(); 
				lastCol.gridwidth = GridBagConstraints.REMAINDER; 
				lastCol.weightx = 1.0; 
				lastCol.fill = GridBagConstraints.HORIZONTAL; 
				lastCol.insets = new Insets(5, 5, 5, 10); 
				JPanel jp7 = new JPanel();
				jp8.setLayout(new GridBagLayout());
				JPanel jp9 = new JPanel();
				jp4.setVisible(false);
				jp5.setVisible(false);
				jp6.setVisible(false);
				jp7.setVisible(true);
				jp8.setVisible(true);
				jp9.setVisible(true);
				
				
				JLabel receiptPage = new JLabel();
				receiptPage.setText("Receipt");
				jp7.add(receiptPage);
				
				String purchaseQtyS1 =  (String) selectQty[0].getSelectedItem();
				String purchaseQtyS2 =  (String) selectQty[1].getSelectedItem();
				String purchaseQtyS3 =  (String) selectQty[2].getSelectedItem();
				String purchaseQtyS4 =  (String) selectQty[3].getSelectedItem();
				String purchaseQtyS5 =  (String) selectQty[4].getSelectedItem();
				String purchaseQtyS6 =  (String) selectQty[5].getSelectedItem();
				String purchaseQtyS7 =  (String) selectQty[6].getSelectedItem();
				String purchaseQtyS8 =  (String) selectQty[7].getSelectedItem();
				int purchasQty1 = Integer.parseInt(purchaseQtyS1);
				int purchasQty2 = Integer.parseInt(purchaseQtyS2);
				int purchasQty3 = Integer.parseInt(purchaseQtyS3);
				int purchasQty4 = Integer.parseInt(purchaseQtyS4);
				int purchasQty5 = Integer.parseInt(purchaseQtyS5);
				int purchasQty6 = Integer.parseInt(purchaseQtyS6);
				int purchasQty7 = Integer.parseInt(purchaseQtyS7);
				int purchasQty8 = Integer.parseInt(purchaseQtyS8);
				int priceIndex1 = 35;
				int priceIndex2 = 125;
				int priceIndex3 = 45;
				int priceIndex4 = 105;
				int priceIndex5 = 105;
				int priceIndex6 = 85;
				int priceIndex7 = 85;
				int priceIndex8 = 280;
				int amount1 = priceIndex1 * purchasQty1;
				int amount2 = priceIndex2 * purchasQty2;
				int amount3 = priceIndex3 * purchasQty3;
				int amount4 = priceIndex4 * purchasQty4;
				int amount5 = priceIndex5 * purchasQty5;
				int amount6 = priceIndex6 * purchasQty6;
				int amount7 = priceIndex7 * purchasQty7;
				int amount8 = priceIndex8 * purchasQty8;
				int totalAmt = amount1 + amount2 + amount3 + amount4 + amount5 + amount6 + amount7 + amount8;
				String amt1 = "$"+amount1+"";
				String amt2 = "$"+amount2+"";
				String amt3 = "$"+amount3+"";
				String amt4 = "$"+amount4+"";
				String amt5 = "$"+amount5+"";
				String amt6 = "$"+amount6+"";
				String amt7 = "$"+amount7+"";
				String amt8 = "$"+amount8+"";
				String total = "$" +totalAmt+"";
				
				String[] itemAmount = {amt1, amt2, amt3, amt4, amt5, amt6, amt7, amt8};
				JLabel[] itemAmt = new JLabel[itemAmount.length];
				for(int i=0; (i<kind.length); i++) { 
	                kinds[i] = new JLabel(); 
	                kinds[i].setText(kind[i]); 
	                jp8.add(kinds[i], firstCol); 
	                itemAmt[i] = new JLabel();
	                itemAmt[i].setText(itemAmount[i]); 
	                itemAmt[i].setPreferredSize(new Dimension(20, 20));
	                jp8.add(itemAmt[i], lastCol);
	            }
				jp8.add(new JLabel(""), firstCol);
				jp8.add(new JLabel(""), lastCol);
				jp8.add(new JLabel(""), firstCol);
				jp8.add(new JLabel(""), lastCol);
				jp8.add(new JLabel(""), firstCol);
				jp8.add(new JLabel(""), lastCol);
				jp8.add(new JLabel("Total Amount"), firstCol);
				jp8.add(new JLabel(total), lastCol);
			
				jp9.add(accept);
				jp9.add(decline);
				accept.setText("Print Receipt");
				decline.setText("Exit from Screen");
				content.setLayout(new BoxLayout(content, BoxLayout.Y_AXIS)); 
				content.add(jp7); 
				content.add(jp8); 
				content.add(jp9); 
				
			}
			if (e.getActionCommand().equals("Print Receipt")) {
				Graphics2D imageGraphics = null;
				try {
					Robot robot = new Robot();
					GraphicsDevice currentDevice = MouseInfo.getPointerInfo().getDevice();
					BufferedImage exportImage = robot.createScreenCapture(currentDevice.getDefaultConfiguration().getBounds());
		 
					imageGraphics = (Graphics2D) exportImage.getGraphics();
					File screenshotFile = new File("./CurrentMonitorScreenshot-"+System.currentTimeMillis()+".png");
					ImageIO.write(exportImage, "png",screenshotFile);
					JOptionPane.showMessageDialog(null, "Screenshot successfully captured to '"+screenshotFile.getCanonicalPath()+"'!");
					
					
				} catch (Exception exp) {
					exp.printStackTrace();
				}
				finally {
					imageGraphics.dispose();
					
				}
			
			}
			if (e.getActionCommand().equals("Exit from Screen")) {
				System.exit(0);
			}
  	
		
			if (e.getActionCommand().equals("Cancel")) {
				System.exit(0);
			}
  	
   
				
		}
}

