import java.awt.Dimension; 
import java.awt.GridBagConstraints; 
import java.awt.GridBagLayout; 
import java.awt.Insets; 
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout; 
import javax.swing.JButton; 
import javax.swing.JFrame; 
import javax.swing.JLabel; 
import javax.swing.JPanel; 
import javax.swing.JTextField; 
public class Authentication extends JFrame { 
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel titlePage; 
	private JLabel[] txt; 
	private JTextField[] jtf; 
	private static JButton accept; 
	private JButton decline; 
	private JPanel jp1; 
	private JPanel jp2; 
	private JPanel jp3; 

	public Authentication() { 
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		setBounds(100, 100, 450 , 300); 
		JPanel content = (JPanel)getContentPane(); 
		GridBagConstraints firstCol = new GridBagConstraints(); 
		firstCol.weightx = 1.0; 
		firstCol.anchor = GridBagConstraints.WEST; 
		firstCol.insets = new Insets(5, 20, 5, 5); 
		GridBagConstraints lastCol = new GridBagConstraints(); 
		lastCol.gridwidth = GridBagConstraints.REMAINDER; 
		lastCol.weightx = 1.0; 
		lastCol.fill = GridBagConstraints.HORIZONTAL; 
		lastCol.insets = new Insets(5, 5, 5, 20); 
		
		String[] labeltxt = {"ID", "Password"}; 
		titlePage = new JLabel("Authentication Page"); 
		txt = new JLabel[2]; 
		jtf = new JTextField[2]; 
		accept = new JButton("Login"); 
		decline = new JButton("Decline"); 
		
		jp1 = new JPanel(); 
		jp2 = new JPanel(new GridBagLayout()); 
		jp3 = new JPanel(); 
		for(int i=0; i<labeltxt.length; i++) { 
			txt[i] = new JLabel(); 
			txt[i].setText(labeltxt[i]); 
			jp2.add(txt[i], firstCol); 
			jtf[i] = new JTextField(); 
			jtf[i].setPreferredSize(new Dimension(300, 20)); 
			jp2.add(jtf[i], lastCol); 
		} 
		jp1.add(titlePage); 
		jp3.add(accept); 
		jp3.add(decline); 
		content.setLayout(new BoxLayout(content, BoxLayout.Y_AXIS)); 
		content.add(jp1); 
		content.add(jp2); 
		content.add(jp3);
		
		}
		 
		public void getid(String id) {
			System.out.println(id);
		}
		
}
		