import java.awt.Dimension; 
import java.awt.GridBagConstraints; 
import java.awt.GridBagLayout; 
import java.awt.Insets; 
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout; 
import javax.swing.JButton; 
import javax.swing.JFrame; 
import javax.swing.JLabel; 
import javax.swing.JPanel; 
import javax.swing.JTextField; 
public class FirstPage extends JFrame implements ActionListener { 
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel titlePage; 
	private JLabel[] txt; 
	private JTextField[] jtf; 
	private static JButton accept; 
	private JButton decline; 
	private JPanel jp1; 
	private JPanel jp2; 
	private JPanel jp3; 
	private String id;
	private String pw;
	
	public FirstPage() { 
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
		setBounds(100, 100, 450 , 300); 
		JPanel content = (JPanel)getContentPane(); 
		GridBagConstraints firstCol = new GridBagConstraints(); 
		firstCol.weightx = 1.0; 
		firstCol.anchor = GridBagConstraints.WEST; 
		firstCol.insets = new Insets(5, 20, 5, 5); 
		GridBagConstraints lastCol = new GridBagConstraints(); 
		lastCol.gridwidth = GridBagConstraints.REMAINDER; 
		lastCol.weightx = 1.0; 
		lastCol.fill = GridBagConstraints.HORIZONTAL; 
		lastCol.insets = new Insets(5, 5, 5, 20); 
				
		String[] labeltxt = {"Name","Account ID","Password","E-Mail","Phone","Address"}; 
		titlePage = new JLabel("Create New Account"); 
		txt = new JLabel[6]; 
		jtf = new JTextField[6]; 
		accept = new JButton("Accept"); 
		decline = new JButton("Decline"); 
		
		jp1 = new JPanel(); 
		jp2 = new JPanel(new GridBagLayout()); 
		jp3 = new JPanel(); 
		for(int i=0; i<labeltxt.length; i++) { 
			txt[i] = new JLabel(); 
			txt[i].setText(labeltxt[i]); 
			jp2.add(txt[i], firstCol); 
			jtf[i] = new JTextField(); 
			jtf[i].setPreferredSize(new Dimension(300, 20)); 
			jp2.add(jtf[i], lastCol); 
		} 
		jp1.add(titlePage); 
		jp3.add(accept); 
		jp3.add(decline); 
		content.setLayout(new BoxLayout(content, BoxLayout.Y_AXIS)); 
		content.add(jp1); 
		content.add(jp2); 
		content.add(jp3); 
		decline.addActionListener(this);
		accept.addActionListener(this);
	}
		public void actionPerformed(ActionEvent e) {
			
		if (e.getActionCommand().equals("Accept")) {
			Authentication frame = new Authentication();
			frame.setVisible(true);
			String id = jtf[1].getText();
			String pw = jtf[2].getText();
		}
		else {
			System.exit(0);
		}
		}
		
		public String getid() {
			return this.id;
		}
		
		public String getpw() {
			return this.pw;
		}
		
   public static void main(String args[]) { 
	   JFrame jf = new FirstPage(); 
	   jf.setVisible(true); 
   }
}
